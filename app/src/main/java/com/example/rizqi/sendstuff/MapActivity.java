package com.example.rizqi.sendstuff;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.rizqi.sendstuff.adapter.PlaceAutocompleteAdapter;
import com.example.rizqi.sendstuff.models.PlaceInfo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompletePrediction;
import com.google.android.gms.location.places.GeoDataClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rizqi on 3/28/2018.
 */

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback,GoogleApiClient.OnConnectionFailedListener ,GoogleMap.OnInfoWindowClickListener {

    private static final String TAG = "MapActivity";
    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSIONS_REQUEST_CODE = 1246;
    private static final LatLngBounds LAT_LNG_BOUNDS = new LatLngBounds(new LatLng(-45,-200),new LatLng(70,100));

    private Boolean mLocationPermissionsGranted = false;
    private GoogleMap mGoogleMap;
    private FusedLocationProviderClient mfusedLocationProviderClient;
    Marker marker;

    private AutoCompleteTextView etSearch;
    private FloatingActionButton btnMyLocation;
    private PlaceAutocompleteAdapter mplaceAutocompleteAdapter;
    private GoogleApiClient mgoogleApiClient;
    private GeoDataClient mGeoDataClient;
    private PlaceInfo mPlace;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        etSearch = findViewById(R.id.et_input_location);
        btnMyLocation = findViewById(R.id.btn_my_location);


        getLocationPermission();

    }

    private void init(){

        mgoogleApiClient = new GoogleApiClient
                .Builder(this)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .enableAutoManage(this, this)
                .build();

        etSearch.setOnItemClickListener(mAutoCompleteClickListener);
        mplaceAutocompleteAdapter = new PlaceAutocompleteAdapter(this,mgoogleApiClient,LAT_LNG_BOUNDS,null);
        etSearch.setAdapter(mplaceAutocompleteAdapter);


        btnMyLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDeviceLocation();
            }
        });

    }

    private void geoLocate(){

        String searchString = etSearch.getText().toString();

        Geocoder geocoder = new Geocoder(MapActivity.this);
        List<Address> list = new ArrayList<>();

        try {
            list = geocoder.getFromLocationName(searchString,1);
        }catch (IOException e){
             Log.e(TAG,"eoexcepton111 : "+ e.getMessage());
        }

        if (list.size() > 0){

            Address address = list.get(0);

            Log.d(TAG, "lokasi ditemukan search:" + address.toString());
//            Toast.makeText(this,address.getLocality().toString(), Toast.LENGTH_SHORT).show();

            moveCamera(new LatLng(address.getLatitude(),address.getLongitude()),15f,address.getThoroughfare());
        }
    }

//

    private void moveCamera(LatLng latLng, float zoom,String title) {

        mGoogleMap.clear();
        Log.d(TAG, "move camera to :" + latLng);
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));

        setMarker(latLng, title);
        marker.showInfoWindow();

    }

    private void setMarker(LatLng latLng, String title) {
        if (marker != null){
            marker.remove();
        }

        MarkerOptions markerOptions = new MarkerOptions()
                .position(latLng)
                .draggable(true)
                .title(title);

        marker =  mGoogleMap.addMarker(markerOptions);
    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);

        mapFragment.getMapAsync(MapActivity.this);
    }

    private void getLocationPermission() {

        String[] permissions = {android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                    COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionsGranted = true;
                initMap();
            } else {
                ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSIONS_REQUEST_CODE);
            }
        } else {
            ActivityCompat.requestPermissions(this, permissions, LOCATION_PERMISSIONS_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        mLocationPermissionsGranted = false;

        switch (requestCode) {
            case LOCATION_PERMISSIONS_REQUEST_CODE: {
                if (grantResults.length > 0) {
                    for (int i = 0; i < grantResults.length; i++) {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED) {
                            mLocationPermissionsGranted = false;
                            return;
                        }
                    }
                    mLocationPermissionsGranted = true;
                    initMap();
                }
            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(this, "READY", Toast.LENGTH_SHORT).show();
        Log.d(TAG, "onMAPReady : map is ready !!");
        mGoogleMap = googleMap;

        mGoogleMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {

                Geocoder gc = new Geocoder(MapActivity.this);
                LatLng ll = marker.getPosition();
                double lat = ll.latitude;
                double lng = ll.longitude;
                List<Address> list = null;
                try {
                    list = gc.getFromLocation(lat,lng,1);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Address add = list.get(0);
                marker.setTitle(add.getAddressLine(0));
                marker.showInfoWindow();
            }
        });

        mGoogleMap.setOnInfoWindowClickListener(this);
        mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(final Marker marker) {
                View v = getLayoutInflater().inflate(R.layout.custom_info_window,null);

                TextView tvJalan = v.findViewById(R.id.tv_address);

                final LatLng ll = marker.getPosition();
                tvJalan.setText(marker.getTitle());

                        Log.d(TAG,"data" +marker.getTitle() + marker.getPosition().longitude + marker.getPosition().latitude);



                return v;
            }
        });

        if (mLocationPermissionsGranted) {
//            getDeviceLocation();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            mGoogleMap.setMyLocationEnabled(true);
           // mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);

            init();
        }
    }

    @Override
    public void onInfoWindowClick(Marker marker) {


        LatLng ll = marker.getPosition();
        double latitude = ll.latitude;
        double longitude = ll.longitude;

        Toast.makeText(this, "Alamat pengiriman yang dituju "  + marker.getTitle(),
                Toast.LENGTH_SHORT).show();

//        Bundle bundle = new Bundle();
//
//        Intent intent = new Intent(getBaseContext(),DetailActivity.class);
//
//        intent.putExtra("title",marker.getTitle());
//        intent.putExtra("long",longitude);
//        intent.putExtra("lat",latitude);
//        startActivity(intent);


    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void hideSoftKeyboard(){
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    private AdapterView.OnItemClickListener mAutoCompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            hideSoftKeyboard();

            final AutocompletePrediction item = mplaceAutocompleteAdapter.getItem(position);
            final String placeId = item.getPlaceId();

            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mgoogleApiClient,placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailCallback);
        }
    };

    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailCallback = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(@NonNull PlaceBuffer places) {
            if (!places.getStatus().isSuccess()){
                Log.d(TAG,"result : Place Resource query not success : " + places.getStatus().toString());
                places.release();
                return;
            }
            final Place place = places.get(0);

            try {

                mPlace = new PlaceInfo();
                mPlace.setId(place.getId());
                mPlace.setName(place.getName().toString());
                mPlace.setAddress(place.getAddress().toString());
                mPlace.setLatLng(place.getLatLng());

                Log.d(TAG,"onResult : " +mPlace.toString());

            }catch (NullPointerException e){
                Log.e(TAG,"on Result : Null Pointer Exception :" + e.getMessage());
            }

            moveCamera(new LatLng(place.getViewport().getCenter().latitude,place.getViewport().getCenter().longitude),15f,mPlace.getAddress());
            places.release();


        }
    };

    private void getGeoLocateDeviceLocation(LatLng latLng){

        Geocoder geocoder = new Geocoder(MapActivity.this);
        List<Address> list = new ArrayList<>();

        try {
            list = geocoder.getFromLocation(latLng.latitude,latLng.longitude,1);
        }catch (IOException e){
            Log.e(TAG,"eoexcepton111 : "+ e.getMessage());
        }

        if (list.size() > 0){

            Address address = list.get(0);

            Log.d(TAG, "lokasi ditemukan search:" + address.toString());
//            Toast.makeText(this,address.getLocality().toString(), Toast.LENGTH_SHORT).show();

            moveCamera(new LatLng(address.getLatitude(),address.getLongitude()),15f,address.getAddressLine(0));
        }

    }

    private void getDeviceLocation() {

        Log.d(TAG, "lokasi device: ");
        mfusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        try {
            if (mLocationPermissionsGranted) {
                final com.google.android.gms.tasks.Task<Location> location = mfusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull com.google.android.gms.tasks.Task<Location> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "lokasi ditemukan");
                            Location currentLocation = (Location) task.getResult();
                            getGeoLocateDeviceLocation(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
//                            moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), 17f,"im here");
                        } else {
                            Log.d(TAG, "lokasi tidak ada");
                        }
                    }
                });
            }

        } catch (SecurityException e) {
            Log.d(TAG, "lokasi device : securtity exception : " + e.getMessage());
        }
    }
}
