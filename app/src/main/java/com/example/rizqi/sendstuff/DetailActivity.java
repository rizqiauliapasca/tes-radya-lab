package com.example.rizqi.sendstuff;

import android.app.Dialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class DetailActivity extends AppCompatActivity {

    private static final String TAG = "DetailActivity";
    private static final int ERROR_DIALOG_REQUEST = 9001;

    TextView tvTitle,tvLat,tvLng;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        tvTitle = findViewById(R.id.tv_title);
        tvLat = findViewById(R.id.tv_lat);
        tvLng = findViewById(R.id.tv_lng);

        Bundle bundle = getIntent().getExtras();

        if (bundle != null){
            tvTitle.setText(bundle.getString("title"));
            tvLat.setText(bundle.getString("lat"));
            tvLng.setText(bundle.getString("long"));
        }

        }

}
